<?php
    require "includes/db.php";
?>
<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <!-- Style -->
        <link rel="stylesheet" href="assets/css/style.css">
        <title>Stickers</title>
    </head>
<!--    <h1><a href="index.php">Stickers</a> will remember everything</h1>-->
    <body>
        <!-- Добавляем адаптивную панель -->
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark sticky-top">
            <a href="#" class="navbar-brand">
                <img src="https://getbootstrap.com/assets/img/bootstrap-stack.png" width="30" height="30" alt="logo">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01"
                    aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
                <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Главная</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Создать стикер</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" data-toggle="modal" data-target="#contactFormModal">Обратная связь</a>
                    </li>
                </ul>
                <ul class="navbar-nav mr-2 mt-2 mt-lg-0">
                    <li class="nav-item">
                        <a class="nav-link" style="color:sandybrown;">
                            <?php if( isset($_SESSION['logged_user']) ) :?>
                                Привет, <?php echo $_SESSION['logged_user']->login; ?>!
                            <?php else: ?>
                                Вы не авторизованы :
                                <!--                            <a href="/login.php">Авторизация</a><br>-->
                                <!--                            <a href="/signup.php">Регистрация</a>-->
                            <?php endif; ?>
                        </a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" style="color:khaki;"
                            <?php if( isset($_SESSION['logged_user']) ) :?> href="/logout.php">Выйти</a>
                            <?php else: ?> href="/login.php">Войти</a>
                            <?php endif; ?>
                    </li>
                </ul>
                <form class="form-inline my-2 my-lg-0">
                    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                </form>
            </div>
        </nav>
        <!-- Добавляем слайдер -->
        <div class="container-fluid cont_carousel-0">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li class="active" data-target="#carouselExampleIndicators" data-slide-to="0"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img src="images/carousel_1.jpeg" alt="Первый слайд" class="d-block w-100">
                        <div class="carousel-caption d-none d-md-block">
                            <h3>Сохраним твои мысли <br> на электронной бумаге - Stickers</h3>
                                <p>поможем не забыть важные события</p>
                            <button class="btn btn-primary btn-lg" onclick='location.href="/signup.php"'>Регистрация</button>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img src="images/carousel_2.jpeg" alt="Второй слайд" class="d-block w-100">
                    </div>
                    <div class="carousel-item">
                        <img src="images/carousel_3.jpeg" alt="Третий слайд" class="d-block w-100">
                    </div>
                </div>
                <a href="#carouselExampleIndicators" class="carousel-control-prev" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a href="#carouselExampleIndicators" class="carousel-control-next" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
        <!-- Контейнер тела -->
        <div class="container-fluid">
            <div class="container">
                <div class="row text-center justify-content-center">
                    <div class="w-100"><br></div>
                    <div class="col-12 col-sm-4 col-lg-3"><img src="images/container_1.jpeg" alt="" class="w-100">
                        <h3>Lorem Ipsum</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium harum in, natus obcaecati possimus quo?</p>
                    </div>
                    <div class="col-12 col-sm-4 col-lg-3"><img src="images/container_2.jpeg" alt="" class="w-100">
                        <h3>Lorem Ipsum</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium harum in, natus obcaecati possimus quo?</p>
                    </div>
                    <div class="col-12 col-sm-4 col-lg-3"><img src="images/container_3.jpeg" alt="" class="w-100">
                        <h3>Lorem Ipsum</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium harum in, natus obcaecati possimus quo?</p>
                    </div>
                    <div class="col-12 col-sm-4 col-lg-3"><img src="images/container_4.jpeg" alt="" class="w-100">
                        <h3>Lorem Ipsum</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium harum in, natus obcaecati possimus quo?</p>
                    </div>

                    <div class="col-12 col-sm-4 col-lg-3"><img src="images/container_5.jpeg" alt="" class="w-100">
                        <h3>Lorem Ipsum</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium harum in, natus obcaecati possimus quo?</p>
                    </div>
                    <div class="col-12 col-sm-4 col-lg-3"><img src="images/container_6.jpg" alt="" class="w-100">
                        <h3>Lorem Ipsum</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium harum in, natus obcaecati possimus quo?</p>
                    </div>
                </div>
            </div>
        </div>

        <?php require "feedback.php"; ?>

        <!-- Optional JavaScript -->
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/script.js"></script>
    </body>
</html>

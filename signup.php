<?php
    require "includes/db.php";

    $data = $_POST;
    if ( isset($data['do_signup']) )
    {
        //регистрируем пользователя

        $errors = array();
        if ( trim($data['login']) == '')
        {
            $errors[] = 'Введите логин!';
        }
        if ( trim($data['email']) == '')
        {
            $errors[] = 'Введите Email!';
        }
        if ( $data['password'] == '')
        {
            $errors[] = 'Введите пароль!';
        }
        if ( $data['password_2'] != $data['password'])
        {
            $errors[] = 'Неверный повторный пароль!';
        }
        if ( R::count('users',"login = ?", array($data['login'])) > 0 )
        {
            $errors[] = 'Пользователь с таким логином уже существует!';
        }
        if ( R::count('users',"email = ?", array($data['email'])) > 0 )
        {
            $errors[] = 'Пользователь с таким Email уже существует!';
        }
        if ( empty($errors))
        {
            //все хорошо, регистрируем
            $user = R::dispense('users');
            $user->login = $data['login'];
            $user->email = $data['email'];
            $user->password = password_hash($data['password'], PASSWORD_DEFAULT);
//            $user->join_date = time();
            R::store($user);
            echo '<div style="color: green;">Вы успешно зарегистрировались!<br>Можете перейти на<a href="/"> главную </a>страницу!</div><hr>';
        } else
        {
            echo '<div style="color: red;">'.array_shift($errors).'</div><hr>';
        }
    }
    ?>

<form action="/signup.php" method="POST">

        <p><strong>Ваш логин</strong>:</p>
    <label>
        <input type="text" name="login" value="<?php echo @$data['login']; ?>">
    </label>

        <p><strong>Ваш Email</strong>:</p>
    <label>
        <input type="email" name="email" value="<?php echo @$data['email']; ?>">
    </label>

        <p><strong>Ваш пароль</strong>:</p>
    <label>
        <input type="password" name="password">
    </label>

        <p><strong>Повторите пароль</strong>:</p>
    <label>
        <input type="password" name="password_2">
    </label>

    <p>
        <button type="submit" name="do_signup">Зарегистрироваться</button>
    </p>
</form>

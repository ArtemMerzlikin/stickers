
<div class="modal fade" id="contactFormModal" tabindex="-1" role="form" aria-labelledby="contactFormModal" aria-hidden="true">
    <div class="modal-dialog" role="dialog">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title" id="#exampleModalLabel">Обратная связь</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputName">Ваше имя</label>
                            <input type="text" class="form-control" id="inputName" placeholder="Имя">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputEmail">Email адрес</label>
                            <input type="email" class="form-control" id="inputEmail" placeholder="Email">
                        </div>
                </div>
                <div class="form-group">
                    <label for="inputSubject">Тема письма</label>
                    <input type="text" class="form-control" id="inputSubject" placeholder="Заголовок">
                </div>
                <div class="form-group">
                    <label for="inputTextArea">Введите текст</label>
                    <textarea name="Test" id="inputTextArea" cols="30" rows="10" class="form-control" placeholder="Введите содержание письма..."></textarea>
                </div>
            </div>

            <div class="modal-footer">
                <button class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button class="btn btn-primary">Отправить письмо</button>
            </div>

        </div>
    </div>
</div>
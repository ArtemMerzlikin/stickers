<?php
    require "includes/db.php";

    $data = $_POST;

    if ( isset($data['do_login']) )
    {
        //авторизация
        $errors = array();
        $user = R::findOne('users', 'login = ?', array($data['login']));
        if ( $user )
        {
            //логин существует
            if ( password_verify($data['password'], $user->password) )
            {
                //все ок, логиним пользователя
                $_SESSION['logged_user'] = $user;
//                echo '<div style="color: green;">Вы авторизованы!<br>Можете перейти на<a href="/"> главную </a>страницу!</div><hr>';
                header('Location: /');
            } else
            {
                $errors[] = 'Неверно введён пароль!';
            }
        } else
        {
            $errors[] = 'Пользователь с таким логином не найден!';
        }
        if ( !empty($errors))
        {
            echo '<div style="color: red;">'.array_shift($errors).'</div><hr>';
        }
    }
?>
<!DOCTYPE html>
<html lang="en" >

    <head>
        <meta charset="UTF-8">
        <title>Login form</title>
        <link rel="stylesheet" href="/assets/css/style.css">
    </head>
    <body>
        <form action="/login.php" method="POST">
            <p><strong>Логин</strong>:</p>
            <label>
                <input type="text" name="login" value="<?php echo @$data['login']; ?>">
            </label>

            <p><strong>Пароль</strong>:</p>
            <label>
                <input type="password" name="password">
            </label>

            <p>
                <button type="submit" name="do_login">Войти</button>
            </p>
        </form>
    </body>
</html>